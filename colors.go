package colors

import (
	"fmt"
	"strconv"
)

const (
	Escape = "\x1b["

	Bell = "\x07"

	ClearScreen  = Escape + "2J"
	ClearLine    = Escape + "K"
	ClearEffects = Escape + "0m"

	SaveCursor    = Escape + "s"
	RestoreCursor = Escape + "u"

	Reset             = "0"
	Bold              = "1"
	Faint             = "2"
	Italic            = "3"
	Underline         = "4"
	SlowBlink         = "5"
	RapidBlink        = "6"
	Swap              = "7"
	Conceal           = "8"
	Strikethrough     = "9"
	DefaultFont       = "10"
	Fraktur           = "20"
	DoubleUnderline   = "21"
	Regular           = "22"
	NoItalic          = "23"
	NoUnderline       = "24"
	NoBlink           = "25"
	NoSwap            = "26"
	NoConceal         = "27"
	NoStrikethrough   = "28"
	DefaultForeground = "39"
	DefaultBackground = "49"
	Framed            = "51"
	Encircle          = "52"
	Overline          = "53"
	NoFrame           = "54"
	NoOverline        = "55"
)

func FourBitColor(color int, fg bool) string {
	if color < 0 || color > 15 {
		return ""
	}

	if color < 8 {
		if fg {
			return strconv.Itoa(color + 30)
		} else {
			return strconv.Itoa(color + 40)
		}
	} else {
		if fg {
			return strconv.Itoa(color - 8 + 90)
		} else {
			return strconv.Itoa(color - 8 + 100)
		}
	}
}

func EightBitColor(color int, fg bool) string {
	if color < 0 || color > 256 {
		return ""
	}

	if fg {
		return "38;5;" + strconv.Itoa(color)
	} else {
		return "48;5;" + strconv.Itoa(color)
	}
}

func RGBColor(r, g, b int, fg bool) string {
	if r < 0 || g < 0 || b < 0 || r > 255 || g > 255 || b > 255 {
		return ""
	}

	if fg {
		return "38;2;" + strconv.Itoa(r) + ";" + strconv.Itoa(g) + ";" + strconv.Itoa(b)
	} else {
		return "48;2;" + strconv.Itoa(r) + ";" + strconv.Itoa(g) + ";" + strconv.Itoa(b)
	}
}

func CombineEffects(effects ...string) string {
	if len(effects) == 0 {
		return ""
	}

	seq := Escape

	if len(effects) == 1 {
		return seq + effects[0] + "m"
	} else {
		// only need ';' after the first effect
		seq += effects[0]
	}

	for _, effect := range effects[1:] {
		seq += ";" + effect
	}

	seq += "m"

	return seq
}

func MoveCursor(line, column int) string {
	return Escape + strconv.Itoa(line) + ";" + strconv.Itoa(column) + "H"
}

func MoveCursorRelative(n int, direction string) string {
	switch direction {
	case "u", "up", "upward", "upwards":
		return Escape + strconv.Itoa(n) + "A"
	case "d", "down", "downward", "downwards":
		return Escape + strconv.Itoa(n) + "B"
	case "l", "b", "left", "back", "leftward", "backward", "leftwards", "backwards":
		return Escape + strconv.Itoa(n) + "D"
	case "r", "f", "right", "front", "rightward", "forward", "rightwards", "forwards":
		return Escape + strconv.Itoa(n) + "C"
	default:
		return ""
	}
}

func MovePrintEffects(line, column int, effects, text string) {
	fmt.Print(MoveCursor(line, column))
	fmt.Print(effects + text)
}

func MovePrint(line, column int, text string) {
	MovePrintEffects(line, column, "", text)
}
