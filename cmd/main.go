package main

import (
	"fmt"
	"gitlab.com/tslnc04/go-colors"
)

func scale(x, xMin, xMax, scaleMin, scaleMax float64) float64 {
	return (scaleMax-scaleMin)/(xMax-xMin)*x + scaleMin
}

func mandelbrot(px, py, pxMax, pyMax int) {
	x0 := scale(float64(px), 0, float64(pxMax), -2.5, 1.0)
	y0 := scale(float64(py), 0, float64(pyMax), -1.0, 1.0)
	x := 0.0
	y := 0.0

	iter := 0

	for x*x+y*y <= 4 && iter < 1000 {
		y = 2*x*y + y0
		x = x*x - y*y + x0
		iter++
	}

	colors.MovePrintEffects(py, px, colors.CombineEffects(colors.RGBColor(256-iter%256, 256-iter%256, 256-iter%256, false)), " ")
}

func main() {
	fmt.Print(colors.ClearScreen)

	for i := 0; i < 300; i++ {
		for j := 0; j < 100; j++ {
			mandelbrot(i, j, 300, 100)
		}
	}
}
