# go-colors

Leverage ANSI escapes to get a colourful terminal

## Usage

There are a two types of effects available as constants in this
library. The first are those that include the entire escape sequence,
such as `ClearScreen` and `SaveCursor`. These must be printed to be
used. However, there is also the second type of effect, such as
`Reset` and `Bold`. Colour effects are also included in this category.
These effects must be passed into `CombineEffects` in order to be
used.

### Colours

Colours can be generated with the three colour functions:
`FourBitColor`, `EightBitColor`, and `RGBColor`. These functions
accept the number of the colour (or the r, g, and b values for
`RGBColor`) and a parameter that when set to true, sets the foreground
colour. These functions return a string that must be passed to
`CombineEffects` in order to work.

#### Example

```go
// This will print "Hello, world!" in red text
fmt.Println(colors.CombineEffects(RGBColor(255, 0, 0, true)) + "Hello, world!")
```

### Text Effects

Text effects are constants that provide a nice mnemonic to the actual
number associated with the effect. To use them, simply add the effect
as an argument to `CombineEffects`. (Bold may also change the colour of
text when using a colour between 0 and 7 [inclusive] with
`FourBitColor`.)

#### Example

```go
// "Hello, world!" will now be printed in bold
fmt.Println(colors.CombineEffects(colors.Bold) + "Hello, world!")
```

### Movement

Movement can be done without printing text using the `MoveCursor` and
`MoveCursorRelative` functions. These functions return an escape code
that must be printed and will move the cursor. `MoveCursor` moves the
cursor to a specific position, specified by the line number then
column number (effectively y then x coordinated). `MoveCursorRelative`
functions the same, but moves the cursor a certain number of cells in
a certain direction. Valid directions are u, up, d, down, l, left, r,
and right. A few more directions are accepted, but it's best just to
go with the simplest.

If text must be printed, there are two functions available. First is
`MovePrintEffects`, which moves to the line and column specified then
prints the effects before the text specified. Next is `MovePrint`,
which is similar, but doesn't have an option for text effects.

#### Examples

```go
// This moves to x: 10, y: 15 and prints "Hello, world!" in bold.
colors.MovePrintEffects(15, 10, colors.CombineEffects(colors.Bold), "Hello, world!")

// This simply moves to line 15, column 10
colors.MoveCursor(15, 10)

// This moves the cursor one position behind where it was
colors.MoveCursorRelative(1, "left")
```
